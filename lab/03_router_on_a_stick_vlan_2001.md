# Introduction

The goal of this lab is to configure a good old fascion router-on-a-stick through an access link and the vlan 2001.

# Topology

```
    +-----+                        +-----+
    |     |  +-------+  +-------+  |     |
    | PC1 +--+  SW1  +--+  R1   +--+ PC2 |
    +-----+  +-------+  +-------+  +-----+
```

# Requirements

* Every devices have the name from the topology
* Disable domain lookup on every devices
* Set vtp mode to transparent
* PC1 is in a dedicated vlan
* vlan 1 is not used
* each vlan has a name
* different IPv4/IPv6 subnets are used in each vlan
* both PC are able to ping each other through IPv4
* both PC are able to ping each other through IPv6
* IPv6 address are assign through prefix delegation
* link between the router and the switch is access link
* the vlan between the router and the switch is 2001 
