# Introduction

The goal of this lab is to explore the Multiple Spanning-Tree algorithm.

# Topology

```
         +-------+
         |  SW1  |
         ++-----++
          |     |
    +-----+-+  ++------+
    |  SW2  +--+  SW3  |
    +-------+  +-------+
```

# Scenario

* Every switches have the name from the topology
* Disable domain lookup on every devices
* Set vtp mode to transparent
* Disable DTP on trunk links, set fixed values and disable negociation.
* vlan 10 and 20 exists on every switches
* MLS is enabled on SW2 and sw3
* Set MLS region name and revision
* RSTP is enabled on SW1
* SW2 is root on MSTI 1, priotity set to 0
* SW3 is secondary on MSTI 1, priotiry set to 4096
* SW2 is secondary root on IST (MST0 or CST), set priority to 4096
* SW3 is root on IST (MST0 or CST), set priority to 0
