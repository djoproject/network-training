# Introduction

The goal of this lab is to use the basic routing function of a Multi-Layer-Switch (MLS).

# Topology

```
       +-------+
       |  SW1  |
       ++----+-+
        |    |
    +----++  ++----+
    |     |  |     |
    | PC1 |  | PC2 |
    +-----+  +-----+
```

# Requirements

* Every devices have the name from the topology
* Disable domain lookup on every devices
* Set vtp mode to transparent
* both PC are in a dedicated vlan
* vlan 1 is not used
* each vlan has a name
* different IPv4/IPv6 subnets are used in each vlan
* both PC are able to ping each other through IPv4
* both PC are able to ping each other through IPv6
* IPv6 address are assign through prefix delegation

# Warnings

* disable ip cef
