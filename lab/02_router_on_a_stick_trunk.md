# Introduction

The goal of this lab is to configure a good old fascion router-on-a-stick through a trunk link.

# Topology

```
        +-------+
        |  R1   |
        +---+---+
            |
        +---+---+
        |  SW1  |
        ++----+-+
         |    |
    +----++  ++----+
    |     |  |     |
    | PC1 |  | PC2 |
    +-----+  +-----+
```

# Requirements
* Every devices have the name from the topology
* Disable domain lookup on every devices
* Set vtp mode to transparent
* Disable DTP on trunk links, set fixed values and disable negociation.
* both PC are in a dedicated vlan
* vlan 1 is not used
* each vlan has a name
* no vlan has a SVI
* different IPv4/IPv6 subnets are used in each vlan
* both PC are able to ping each other through IPv4
* both PC are able to ping each other through IPv6
* IPv6 address are assign through prefix delegation
* link between the router and the switch is a trunk
* only vlan created for PC are carried on trunk
* native vlan on trunk is not the vlan 1
