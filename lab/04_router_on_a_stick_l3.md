# Introduction

The goal of this lab is to configure a good old fascion router-on-a-stick through a L3 link.

# Topology

```
    +-----+                        +-----+
    |     |  +-------+  +-------+  |     |
    | PC1 +--+  SW1  +--+  R1   +--+ PC2 |
    +-----+  +-------+  +-------+  +-----+
```

# Requirements

* The switch and the router have the hosname described in topology
* PC1 is in a dedicated vlan
* vlan 1 is not used
* each vlan has a name
* different IPv4/IPv6 subnets are used in each vlan
* both PC are able to ping each other through IPv4
* both PC are able to ping each other through IPv6
* IPv6 address are assign through prefix delegation
* link between the router and the switch is L3 link 

# Warnings

* disable ip cef
