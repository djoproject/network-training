# Introduction

The goal of this lab is to use Vlan Trunking Protocol (VTP).

# Topology

```
    +-------+  +-------+  +-------+
    |  SW1  +--+  SW2  +--+  SW3  |
    +-------+  +-------+  +-------+
```

# Scenario

* Every devices have the name from the topology
* Disable domain lookup on every devices
* Every switch are linked through trunk links
* Disable DTP on trunk links, set fixed values and disable negociation.
* Disable both LLDP and CDP
* Set VTP version 3
* SW1 is VTP primary server
* SW2 is VTP transpartent
* SW3 is VTP client
* Create a vlan on SW1
* This vlan should not exist on SW2
* This vlan must appear on SW3
