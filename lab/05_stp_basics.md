# Introduction

The goal of this lab is to configure a good old-fascioned switch triangle with Rapid-Spanning-Tree-Protocol (RSTP)

# Introduction

```
         +-------+
         |  SW1  |
         ++-----++
          |     |
    +-----+-+  ++------+
    |  SW2  +--+  SW3  |
    +-------+  +-------+
```

# Requirement
* Every devices have the name from the topology
* Disable domain lookup on every devices
* Set vtp mode to transparent
* Set spanning-tree mode to RSTP
* use extended id
* use path cost long method
* set portfast on edge interface
* set bpduguard on edge interface
* SW1 must explicitly be primary root
* SW2 must explicitly be the secondary root
