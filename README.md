# network-training

## Introduction

I often lack of network training through my jobs but I need to stay trained for when it is needed.  Through these short labs I tried to be able to quickly repeat short configuration.

The goal is not to make difficult configuration or tricky situation, but to repeat standard processes.

## Labs

* [01_mls_routing](./lab/01_mls_routing.md)
* [02_router_on_a_stick_trunk](./lab/02_router_on_a_stick_trunk.md)
* [03_router_on_a_stick_vlan_2001](./lab/03_router_on_a_stick_vlan_2001.md)
* [04_router_on_a_stick_l3](./lab/04_router_on_a_stick_l3.md)
* [05_stp_basics](./lab/05_stp_basics.md)
* [06_mls](./lab/06_mst.md)
* [07_vtp](./lab/07_vtp.md)
